package com.gabriel.CryptoGallery2.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gabriel.CryptoGallery2.PhotoCrypt;
import com.example.ultralord.myapplication.R;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.IOException;

public class PhotoImage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_image);

        PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);
        Intent intent = getIntent();
        String name = intent.getStringExtra("Name");
        long id = intent.getLongExtra("id", 0);

        Bitmap bitmapPhoto = null;
        try {
            bitmapPhoto = PhotoCrypt.getPhotoBitmap(name, id, getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        photoView.setImageDrawable(new BitmapDrawable(getResources(), bitmapPhoto));
    }
}
