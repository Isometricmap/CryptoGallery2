package com.gabriel.CryptoGallery2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PhotoDatabase extends SQLiteOpenHelper {

    public static final int DATABASASE_VERSION = 1;
    public static final String DATABASE_NAME = "Photo Database";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Photo.PhotoEntry.TABLE_NAME + " (" +
                    Photo.PhotoEntry._ID + " INTEGER PRIMARY KEY," +
                    Photo.PhotoEntry.PHOTO_NAME + " TEXT," +
                    Photo.PhotoEntry.ALBUM_NAME + " TEXT," +
                    Photo.PhotoEntry.NONCE + " INTEGER," +
                    Photo.PhotoEntry.THUMBNAIL_DATA + " BLOB," +
                    Photo.PhotoEntry.PHOTO_LOCATION + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Photo.PhotoEntry.TABLE_NAME;

    public PhotoDatabase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(SQL_CREATE_ENTRIES);

    }

    //worry about this later
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}