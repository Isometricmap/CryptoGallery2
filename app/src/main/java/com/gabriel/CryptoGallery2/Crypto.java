
package com.gabriel.CryptoGallery2;
import android.util.Base64;


import com.goterl.lazycode.lazysodium.*;
import com.goterl.lazycode.lazysodium.interfaces.PwHash;
import com.goterl.lazycode.lazysodium.interfaces.SecretBox;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;


public class Crypto {

    private static byte[] key, nonce;

    public static void setUserKet(String userKey) throws IOException
    {
        key = genKey(userKey);
    }

    public static byte[] encryptBuffer(byte[] buffer, long id) throws IOException
    {
        byte[] cipherText = new byte[buffer.length + SecretBox.MACBYTES];
        byte[] newNonce = new byte[SecretBox.NONCEBYTES];

        SodiumAndroid na = new SodiumAndroid();
        LazySodium lazyNa = new LazySodiumAndroid(na, StandardCharsets.UTF_8);
        SecretBox.Native secretbox = (SecretBox.Native) lazyNa;

        if (nonce == null) {
            //
        } else {
            System.out.println("Used id to encrypt " + id);
            newNonce = addLongtoBytes(nonce, id);
        }


        boolean b = secretbox.cryptoSecretBoxEasy(cipherText, buffer, buffer.length, newNonce, key); //maybe error check?

        System.out.println("Encryption result: " + b);

        return cipherText;

    }

    public static byte[] decryptBuffer(byte[] buffer, long id)
    {
        byte[] newNonce;
        byte[] output = new byte[buffer.length];

        SodiumAndroid na = new SodiumAndroid();
        LazySodium lazyNa = new LazySodiumAndroid(na, StandardCharsets.UTF_8);
        SecretBox.Native secretbox = (SecretBox.Native) lazyNa;

        System.out.println("Used id to decrypt: " + id);
        newNonce = addLongtoBytes(nonce, id);
        System.out.println("Decrypt - Using nonce: " + id);

        boolean b = secretbox.cryptoSecretBoxOpenEasy(output, buffer, buffer.length, newNonce, key);
        System.out.println("Decrypt status: " + b);

        return output;
    }

    private static byte[] genKey(String userKey) throws IOException
    {

        byte[] userKeyStr_to_bytes = userKey.getBytes();
        byte[] salt;

        SodiumAndroid na = new SodiumAndroid();
        LazySodium lazyNa = new LazySodiumAndroid(na, StandardCharsets.UTF_8);
        PwHash.Native hasher = (PwHash.Native) lazyNa;


        key = new byte[SecretBox.KEYBYTES];

        if (!FileHelper.checkIVandSalt()) {
            nonce = lazyNa.randomBytesBuf(SecretBox.NONCEBYTES);
            salt = lazyNa.randomBytesBuf(PwHash.ARGON2ID_SALTBYTES);
            FileHelper.storeNonceandSalt(nonce, salt);
        } else {
            salt = FileHelper.readNonceandSalt().salt;
            nonce = FileHelper.readNonceandSalt().nonce;
        }

        boolean success = hasher.cryptoPwHash(key, key.length, userKeyStr_to_bytes, userKeyStr_to_bytes.length, salt, PwHash.ARGON2ID_OPSLIMIT_INTERACTIVE, PwHash.ARGON2ID_MEMLIMIT_INTERACTIVE, PwHash.Alg.PWHASH_ALG_ARGON2ID13);
        System.out.println("pw hash: " +success);
        return key;
    }

    public static String genName()
    {
        SodiumAndroid na = new SodiumAndroid();
        LazySodium lazyNa = new LazySodiumAndroid(na, StandardCharsets.UTF_8);

        byte[] randBytes = lazyNa.randomBytesBuf(13);
        String base64 = Base64.encodeToString(randBytes, Base64.DEFAULT);

        return base64;

    }

    //this is probably not well thought out
    public static byte[] addLongtoBytes(byte[] baseNonce, long entryNumb)
    {
        int tmp = 0;
        int carryD = 0;
        int carryR = 0;

        ByteBuffer longBuffer = ByteBuffer.allocate(Long.SIZE);

        byte[] newNonce = new byte[baseNonce.length];
        byte[] longBytes = new byte[Long.SIZE];
        longBuffer.putLong(entryNumb);
        longBuffer.flip();

        longBytes = longBuffer.array();

        for (int i = (baseNonce.length - 1); i > 0; i--) {
            tmp = (baseNonce[i] & 0xFF) + (longBytes[i + (longBytes.length - baseNonce.length)] & 0xFF)+ carryD ;
            carryD = tmp / 255;
            carryR = tmp % 256;

            newNonce[i] = (byte) carryR;

        }

        return newNonce;
    }


}
