package com.gabriel.CryptoGallery2;

import android.provider.BaseColumns;

public class Photo {

    private byte[] thumbnail;
    private String location;
    private String name;
    private String album;
    private long nonce;

    public Photo(String name, String location, String album, long nonce, byte[] thumbnail)
    {
        this.location = location;
        this.name = name;
        this.thumbnail = thumbnail;
        this.album = album;
        this.nonce = nonce;
    }

    public byte[] getThumbnail()
    {
        return this.thumbnail;
    }

    public String getLocation()
    {
        return this.location;
    }

    public String getName()
    {
        return this.name;
    }

    public String getAlbum()
    {
        return this.album;
    }

    public long getNonce() { return this.nonce; }

    public static class PhotoEntry implements BaseColumns {
//        public static final String _ID = "ID";
        public static final String TABLE_NAME = "Photos";
        public static final String PHOTO_NAME = "Name";
        public static final String PHOTO_LOCATION = "Location";
        public static final String THUMBNAIL_DATA = "Thumbnail";
        public static final String ALBUM_NAME = "Album_name";
        public static final String NONCE = "Nonce";
    }

    public static class AlbumEntry implements  BaseColumns {
        public static final String TABLE_NAME = "Albums";
        public static final String ALBUM_NAME = "Name";
        public static final String COVER_PHOTO = "Photo";
    }

}
