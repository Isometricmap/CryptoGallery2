package com.gabriel.CryptoGallery2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.gabriel.CryptoGallery2.Activities.MainGallery;
import com.example.ultralord.myapplication.R;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final int READ_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FileHelper.getDirectory(getApplicationContext());


    }


    public void setPass(View v) throws IOException
    {
        EditText textfield = (EditText) findViewById(R.id.PassField);
        String pass =  textfield.getText().toString();
        Crypto.setUserKet(pass);

        Intent myIntent = new Intent(MainActivity.this, MainGallery.class);
        MainActivity.this.startActivity(myIntent);
    }



}

