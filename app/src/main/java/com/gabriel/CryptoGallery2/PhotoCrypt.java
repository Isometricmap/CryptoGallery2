package com.gabriel.CryptoGallery2;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;


public class PhotoCrypt {

    //class for UI interface

    //add photo to both DB and internal storage, don't delete the original
    public static void addPhoto(Photo photo, Uri uri, Context context) throws IOException
    {
        String name = photo.getName();
        DatabaseHandler.addPhoto(photo, context);
        FileHelper.copyFile(uri, name, photo.getNonce(), context);
        System.out.println("I'm here!~!~!~!~!~!");
    }

    public static void addAlbum(String photoName, String album, Context context)
    {
        DatabaseHandler.addAlbum(photoName, album, context);
    }

    //used to get thumbnails for an album
    public static ArrayList<ByteBuffer> getThumbs(String album, Context context)
    {
        ArrayList<Photo> photos = DatabaseHandler.getPhotobyAlbum(album, context);
        ArrayList<ByteBuffer> thumbs = new ArrayList<>();

        for (int i = 0; i < photos.size(); i++) {
            Photo currPhoto = photos.get(i);
            byte[] thumbData = currPhoto.getThumbnail();
            ByteBuffer thumbBuffer = ByteBuffer.wrap(thumbData);
            thumbs.add(thumbBuffer);
        }
        return thumbs;
    }

    public static ArrayList<Photo> getPhotosbyAlbum(String album, Context context)
    {
        return DatabaseHandler.getPhotobyAlbum(album, context);
    }

    public static ArrayList<ByteBuffer> getAlbumThumbs(Context context)
    {
        ArrayList<Photo> photos = getAlbums(context);
        ArrayList<ByteBuffer> thumbs = new ArrayList<>();

        for (int i = 0; i < photos.size(); i++) {
            Photo photo = photos.get(i);
            if (photo != null) {
                Photo fullPhoto = DatabaseHandler.getPhotobyName(photo.getName(), context);
                byte[] thumbData = fullPhoto.getThumbnail();
                ByteBuffer thumbBuffer = ByteBuffer.wrap(thumbData);
                thumbs.add(thumbBuffer);
            }

        }

        return thumbs;
    }

    public static ArrayList<ByteBuffer> getThumbs(ArrayList<Photo> photos, Context context)
    {
        ArrayList<ByteBuffer> thumbs = new ArrayList<>();

        for (int i = 0; i < photos.size(); i++) {
            Photo currPhoto = photos.get(i);
            byte[] thumbData = currPhoto.getThumbnail();
            ByteBuffer thumbBuffer = ByteBuffer.wrap(thumbData);
            thumbs.add(thumbBuffer);
        }

        return thumbs;
    }

    public static ArrayList<Photo> getAlbums(Context context)
    {
        return DatabaseHandler.getAlbumCovers(context);
    }


public static Bitmap getPhotoBitmap(String name, long id, Context context) throws IOException
{
    return FileHelper.getPhoto(name, id, context);
}

//    public static void



}
