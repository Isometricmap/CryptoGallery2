package com.gabriel.CryptoGallery2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class DatabaseHandler {

    public static void addPhoto(Photo photo, Context context) throws IOException {

        PhotoDatabase DBhelper = new PhotoDatabase(context);
        SQLiteDatabase db = DBhelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        long id = photo.getNonce();
        byte[] encryptedThumbnail = Crypto.encryptBuffer(photo.getThumbnail(), id);

        values.put(Photo.PhotoEntry.PHOTO_LOCATION, photo.getLocation());
        values.put(Photo.PhotoEntry.PHOTO_NAME, photo.getName());
        values.put(Photo.PhotoEntry.ALBUM_NAME, photo.getAlbum());
        values.put(Photo.PhotoEntry.NONCE, id);

        values.put(Photo.PhotoEntry.THUMBNAIL_DATA, encryptedThumbnail);

        long RowID = db.insert(Photo.PhotoEntry.TABLE_NAME, null, values);



        // do some error checking here
        if (RowID == -1) Log.d("oh fuck", "addPhoto fucked");
    }

    public static void deletePhoto(Photo photo, Context context)
    {
        PhotoDatabase DBhelper = new PhotoDatabase(context);
        SQLiteDatabase db = DBhelper.getWritableDatabase();

        String selection = Photo.PhotoEntry.PHOTO_NAME + " LIKE ?";
        String[] selectionArgs = {photo.getName()};

        int deleted = db.delete(Photo.PhotoEntry.TABLE_NAME, selection, selectionArgs);

        if (deleted == 0) {
            //do some logging here
        }
    }

    public static void deleteAlbum(String album, Context context)
    {
        PhotoDatabase DBhelper = new PhotoDatabase(context);
        SQLiteDatabase db = DBhelper.getWritableDatabase();

        String selection = Photo.PhotoEntry.ALBUM_NAME + " LIKE ?";
        String[] selectionArgs = {album};

        int deleted = db.delete(Photo.PhotoEntry.TABLE_NAME, selection, selectionArgs);

        if (deleted == 0) {
            //do some logging here
        }
    }

    public static Photo getPhotobyName(String name, Context context)
    {
        long id;
        String photo_name, album, location;

        PhotoDatabase DBhelper = new PhotoDatabase(context);
        SQLiteDatabase db = DBhelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                Photo.PhotoEntry.PHOTO_NAME,
                Photo.PhotoEntry.ALBUM_NAME,
                Photo.PhotoEntry.THUMBNAIL_DATA,
                Photo.PhotoEntry.PHOTO_LOCATION,
                Photo.PhotoEntry.NONCE
        };

        String selection = Photo.PhotoEntry.PHOTO_NAME + " = ?";
        String[] selectionArgs = {name};
        String sortOrder = Photo.PhotoEntry.PHOTO_NAME + " DESC";

        Cursor cursor = db.query(
                Photo.PhotoEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        if (cursor.moveToNext()) {

            byte[] plaintextThumb, encryptedThumbnail;

            id = cursor.getLong(cursor.getColumnIndex("Nonce"));
            location = cursor.getString(cursor.getColumnIndex("Location"));
            album = cursor.getString(cursor.getColumnIndex("Album_name"));
            encryptedThumbnail = cursor.getBlob(cursor.getColumnIndex("Thumbnail"));
            name = cursor.getString(cursor.getColumnIndex("Name"));
            plaintextThumb = Crypto.decryptBuffer(encryptedThumbnail, id); //maybe check to see if there was as decryption error
            cursor.close();

            return new Photo(name, location, album, id,  plaintextThumb);

        } else {
            //do some error logging here
            return null;
        }

    }

    public static ArrayList<Photo> getPhotobyAlbum(String album, Context context)
    {
        long id;
        String name, location;
        byte[] encryptedThumbnail;
        ArrayList<Photo> photos = new ArrayList<>();

        PhotoDatabase DBhelper = new PhotoDatabase(context);
        SQLiteDatabase db = DBhelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                Photo.PhotoEntry.PHOTO_NAME,
                Photo.PhotoEntry.ALBUM_NAME,
                Photo.PhotoEntry.THUMBNAIL_DATA,
                Photo.PhotoEntry.PHOTO_LOCATION,
                Photo.PhotoEntry.NONCE
        };

        System.out.println("ALBUM SERCHING: " + album);
        String selection = Photo.PhotoEntry.ALBUM_NAME + " = ?";
        String[] selectionArgs = {album};
        String sortOrder = Photo.PhotoEntry.ALBUM_NAME + " DESC";

        Cursor cursor = db.query(
                Photo.PhotoEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        while (cursor.moveToNext()) {

            byte[] plaintextThumb;

            id = cursor.getLong(cursor.getColumnIndex("Nonce"));
            location = cursor.getString(cursor.getColumnIndex("Location"));
            encryptedThumbnail = cursor.getBlob(cursor.getColumnIndex("Thumbnail"));
            name = cursor.getString(cursor.getColumnIndex("Name"));
            plaintextThumb = Crypto.decryptBuffer(encryptedThumbnail, id); //maybe check to see if there was as decryption error

            photos.add(new Photo(name, location, album, id, plaintextThumb));

        }

        cursor.close();

        return photos;
    }

    public static long addAlbum(String photoName, String album, Context context)
    {
        AlbumDatabase DBhelper = new AlbumDatabase(context);
        SQLiteDatabase db = DBhelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Photo.AlbumEntry.ALBUM_NAME, album);
        values.put(Photo.AlbumEntry.COVER_PHOTO, photoName);

        long RowID = db.insert(Photo.AlbumEntry.TABLE_NAME, null, values);

        return RowID;
    }

    public static ArrayList<Photo> getAlbumCovers(Context context)
    {
        String name;
        ArrayList<Photo> photos = new ArrayList<>();

        AlbumDatabase DBhelper = new AlbumDatabase(context);
        SQLiteDatabase db = DBhelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                Photo.AlbumEntry.ALBUM_NAME,
                Photo.AlbumEntry.COVER_PHOTO,
        };

        Cursor cursor = db.query(
                Photo.AlbumEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        while (cursor.moveToNext()) {
            name = cursor.getString(cursor.getColumnIndex("Photo"));
            System.out.println("album name: " + name);
            Photo photo = DatabaseHandler.getPhotobyName(name, context);
            photos.add(photo);
        }

        cursor.close();

        return photos;
    }

    public static long countEntries(Context context)
    {
        PhotoDatabase DBhelper = new PhotoDatabase(context);
        SQLiteDatabase db = DBhelper.getReadableDatabase();

        return DatabaseUtils.queryNumEntries(db, Photo.PhotoEntry.TABLE_NAME);
    }

    public static long countAlbumEntries(Context context)
    {
        AlbumDatabase DBhelper = new AlbumDatabase(context);
        SQLiteDatabase db = DBhelper.getReadableDatabase();

        return DatabaseUtils.queryNumEntries(db, Photo.AlbumEntry.TABLE_NAME);
    }




}
