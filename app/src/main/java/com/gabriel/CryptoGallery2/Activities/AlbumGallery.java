package com.gabriel.CryptoGallery2.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.gabriel.CryptoGallery2.AlbumDatabase;
import com.gabriel.CryptoGallery2.Crypto;
import com.gabriel.CryptoGallery2.FileHelper;
import com.gabriel.CryptoGallery2.Photo;
import com.gabriel.CryptoGallery2.PhotoCrypt;
import com.example.ultralord.myapplication.R;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;

public class AlbumGallery extends AppCompatActivity {

    String album;
    ArrayList<Photo> photos;
    TableLayout table;
    public static final int READ_REQUEST_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_gallery);
        table = (TableLayout) findViewById(R.id.GalleryTable);
        album = getIntent().getStringExtra("Album_Name");
        photos = PhotoCrypt.getPhotosbyAlbum(album, getApplicationContext());

        populateGallery(table);

    }

    private boolean populateGallery(TableLayout table) //this needs to be threaded
    {
        int thumbCount = 0;
        TableRow row;
        int albumCount, rowCount, photosPerRow;
        int pixelWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        ArrayList<Photo> photos = PhotoCrypt.getPhotosbyAlbum(album, getApplicationContext());
        ArrayList<ByteBuffer> thumbs = PhotoCrypt.getThumbs(photos, getApplicationContext());

        albumCount = photos.size();

        photosPerRow = pixelWidth / FileHelper.THUMBNAIL_SIZE;
        rowCount = albumCount / photosPerRow;

        if ( albumCount % photosPerRow > 0 ) {
            rowCount++;
        }

        for (int i = 0; i < rowCount; i++) {
            row = new TableRow(this);
            for (int j = 0; j < photosPerRow; j++) {
                if (thumbCount < thumbs.size()) {
                    final Photo photo = photos.get(thumbCount);
                    ByteBuffer thumbBuffer = thumbs.get(thumbCount);
                    byte[] thumbArray = thumbBuffer.array();

                    Bitmap thumbnail = BitmapFactory.decodeByteArray(thumbArray, 0, thumbArray.length);
                    ImageButton img = new ImageButton(this);
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent imageActivity = new Intent(AlbumGallery.this, PhotoImage.class);
                            imageActivity.putExtra("Name", photo.getName());
                            imageActivity.putExtra("id", photo.getNonce());
                            AlbumGallery.this.startActivity(imageActivity);
                        }
                    });
                    img.setImageBitmap(thumbnail);
                    row.addView(img);
                    thumbCount++;
                }
            }
            table.addView(row);
        }

        return true;
    }

    public void addPhoto(View v)
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {


        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK)
        {
            super.onActivityResult(requestCode, resultCode, resultData);


            AlbumGallery.MovePhotosTask photoMoveThread = new AlbumGallery.MovePhotosTask();
            photoMoveThread.execute(resultData);
            recreate();
        }
    }

    private class MovePhotosTask extends AsyncTask<Intent, Void, String>
    {

        @Override
        protected String doInBackground(Intent... resultData)
        {
            Uri uri;
            uri = resultData[0].getData();
            byte[] thumb;

            try {
                //check for name collision, try encrpytion
                Random rand = new Random(); //shouldn't really need to be secure, just need a somewhat random value
                long id = (long) rand.nextInt(Integer.MAX_VALUE);
                AlbumDatabase db = new AlbumDatabase(getApplicationContext());
                String name = Crypto.genName();
                thumb = FileHelper.getThumbnail(uri, getApplicationContext());

                Photo photo = new Photo(name, null, album, id, thumb);
                PhotoCrypt.addPhoto(photo, uri, getApplicationContext());

                return "Files copied successfully";

            } catch (IOException e) {
                return "Failure to copy files";
            }
        }
    }
}
