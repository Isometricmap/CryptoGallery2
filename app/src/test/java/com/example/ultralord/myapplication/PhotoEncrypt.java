package com.example.ultralord.myapplication;

import com.example.ultralord.CryptoGallery2.Crypto;
import com.goterl.lazycode.lazysodium.interfaces.SecretBox;

import org.junit.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import static org.junit.Assert.*;

//make a better test
public class PhotoEncrypt {

    @Test
    public void addLongtoByteTest()
    {
        long x = 265423;
        long y = 255;

        byte[] x_bytes =  ByteBuffer.allocate(64).putLong(x).array();
        byte[] out1 = ByteBuffer.allocate(64).putLong(x + y).array();

        byte[] out = Crypto.addLongtoBytes(x_bytes,  y);

        ByteBuffer buffer = ByteBuffer.allocate(out.length);
        buffer.put(out);
        buffer.flip();//need flip
        long outLong = buffer.getLong();

        assertArrayEquals(out, out1);

    }

    @Test
    public void EncryptDecrpyt() throws IOException
    {
        long id = 10;
        Random rand = new Random();

        byte[] testString = new byte[100000];
        byte[] outputBuffer = new byte[testString.length + SecretBox.MACBYTES];
        String userKey = "test";
        String outputString;

        rand.nextBytes(testString);
        byte[] encryptedBuffer, decrpytedBuffer, decryptedTruncated, inputbuffer;

        Crypto.genKey(userKey);

        for (int i = 4096; i < testString.length; i = i + 4096) {

            inputbuffer = new byte[4096];
            for (int j = 0; j < 4096; j++) {
                if (i + j >= testString.length) {
                    break;
                }
                inputbuffer[i] = testString[i + j];
            }
            encryptedBuffer = Crypto.encryptBuffer(inputbuffer, id);
        }
//        encryptedBuffer = Crypto.encryptBuffer(inputBuffer, id);
//        decrpytedBuffer = Crypto.decryptBuffer(encryptedBuffer, id);


//        decryptedTruncated = new byte[decrpytedBuffer.length - SecretBox.MACBYTES];

//        for (int i = 0; i < decryptedTruncated.length; i++) {
//            decryptedTruncated[i] = decrpytedBuffer[i];
//        }
//        outputString = new String(decryptedTruncated, "UTF-8");

//        assertArrayEquals(inputBuffer, decryptedTruncated);

    }

}
