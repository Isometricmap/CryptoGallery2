package com.gabriel.CryptoGallery2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//could probably use an interface for the database classes, but we only need two
public class AlbumDatabase extends SQLiteOpenHelper {

    public static final int DATABASASE_VERSION = 1;
    public static final String DATABASE_NAME = "Album Database";


    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Photo.AlbumEntry.TABLE_NAME + " (" +
                    Photo.AlbumEntry._ID + " INTEGER PRIMARY KEY," +
                    Photo.AlbumEntry.ALBUM_NAME + " TEXT," +
                    Photo.AlbumEntry.COVER_PHOTO + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Photo.AlbumEntry.TABLE_NAME;

    public AlbumDatabase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(SQL_CREATE_ENTRIES);

    }

    //worry about this later
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}