package com.gabriel.CryptoGallery2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import com.goterl.lazycode.lazysodium.interfaces.PwHash;
import com.goterl.lazycode.lazysodium.interfaces.SecretBox;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileHelper {

    private static File FILENAME;
    private static final int NONCE_LENGTH = SecretBox.NONCEBYTES;
    private static final int SALT_LENGTH = PwHash.SALTBYTES;
    private static File PHOTO_DIR;
    public static final int THUMBNAIL_SIZE = 425;
    private static final int BUFFER_SIZE = 4092;

    public static boolean checkIVandSalt()
    {
        if (FILENAME.length() > 0) {
            return true;
        }
        return false;
    }

    public static void getDirectory(Context context)
    {
        PHOTO_DIR = context.getDir("Photos", Context.MODE_PRIVATE);
        FILENAME = new File(context.getFilesDir(), "IVandSalt");
    }

    public static returnBytes readNonceandSalt() throws IOException
    {
        File file = FILENAME;
        FileInputStream filein = new FileInputStream(file);

        byte[] nonce = new byte[NONCE_LENGTH];
        byte[] salt = new byte[SALT_LENGTH];

        if (filein.read(nonce) != NONCE_LENGTH) {
            //another problem here
        }
        if (filein.read(salt) != SALT_LENGTH) {
            //same
        }

        filein.close();
        return new returnBytes(nonce, salt);
    }

    public static void storeNonceandSalt(byte[] nonce, byte[] salt) throws IOException
    {
        File file = FILENAME;
        FileOutputStream fileout = new FileOutputStream(file);
        fileout.write(nonce);
        fileout.write(salt);
        fileout.close();
    }

    //used to read salt and nonce in one function
    public static class returnBytes {

        public byte[] nonce, salt;

        public returnBytes(byte[] nonce, byte[] salt)
        {
            this.nonce = nonce;
            this.salt = salt;
        }
    }

    //copys and encrypts file into internal storage - uses byte streams for simplicity - test lines can be removed
    public static void copyFile(Uri photoUri, String newName, Long id,  Context context) throws IOException
    {
        //move file to internal storage
        int remainingBytes;
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(photoUri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();

        FileInputStream filein = new FileInputStream(fileDescriptor);

        FileOutputStream encryptedOutputFileStream = new FileOutputStream(new File(PHOTO_DIR, newName));
        FileOutputStream outFileTest = new FileOutputStream(new File(PHOTO_DIR, newName + ".orig")); //for testing
        FileOutputStream outTest = new FileOutputStream(new File(PHOTO_DIR, newName + ".test"));//for testing

        byte[] buffer = new byte[BUFFER_SIZE];

        ByteArrayOutputStream plainTextOutputByteStream = new ByteArrayOutputStream(); //rename these

        while (filein.read(buffer) > 0) {
            outFileTest.write(buffer); //testing - the original file
            plainTextOutputByteStream.write(buffer);
        }

        ByteArrayInputStream encryptedInputByteStream = new ByteArrayInputStream(Crypto.encryptBuffer(plainTextOutputByteStream.toByteArray(), id));

        while ((remainingBytes = encryptedInputByteStream.available()) > 0) {

            if (remainingBytes < BUFFER_SIZE) {
                encryptedInputByteStream.read(buffer, 0, remainingBytes);
                encryptedOutputFileStream.write(buffer, 0, remainingBytes);
            } else {
                encryptedInputByteStream.read(buffer, 0, BUFFER_SIZE);
                encryptedOutputFileStream.write(buffer);
            }
        }

//        //start testing
//        filein = new FileInputStream(new File(PHOTO_DIR, newName));
//        ByteArrayOutputStream encryptedPhotoArray = new ByteArrayOutputStream();
//
//        int rc;
//        while ((rc = filein.read(buffer)) > 0) {
//            encryptedPhotoArray.write(buffer,0, rc);
//        }
//
//        byte[] encrypedPhotoBytes = encryptedPhotoArray.toByteArray();                      //read encrypted buffer
//        byte[] encryptedPhotoByteArray = Crypto.decryptBuffer(encrypedPhotoBytes, id);
//
//        ByteArrayInputStream encryptedTestByteStream = new ByteArrayInputStream(encryptedPhotoByteArray);
//
//        while ((remainingBytes = encryptedTestByteStream.available()) > 0) {
//            if (remainingBytes < BUFFER_SIZE) {
//                encryptedTestByteStream.read(buffer, 0, remainingBytes);
//                outTest.write(buffer, 0, remainingBytes);
//            } else {
//                encryptedTestByteStream.read(buffer);
//                outTest.write(buffer);
//            }
//        }
    }

//    public static Bitmap getBitmap(ByteArrayInputStream b)
//    {
//        InputStream input = (ByteArrayInputStream) b;
//    }

    //
    public static byte[] getThumbnail(Uri photoUri, Context context) throws FileNotFoundException //
    {
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(photoUri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap resized = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFileDescriptor(fileDescriptor), THUMBNAIL_SIZE, THUMBNAIL_SIZE);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.PNG, 100, stream);

        return stream.toByteArray();

    }

    public static Bitmap getPhoto(String name, long id, Context context) throws IOException
    {
//        ByteBuffer plaiTextOutput = new ByteBuffer();
        byte[] buffer = new byte[BUFFER_SIZE];
        ByteArrayOutputStream enryptedOutput = new ByteArrayOutputStream();
        FileInputStream filein = new FileInputStream(new File(PHOTO_DIR, name)); //may need to give it the correct directory

        int rc;
        while ((rc = filein.read(buffer)) > 0) {
            enryptedOutput.write(buffer,0, rc);
        }

        byte[] encryptedBuffer = enryptedOutput.toByteArray();
        byte[] decryptedBuffer = Crypto.decryptBuffer(encryptedBuffer, id);

//        ByteArrayInputStream decryptedByteStream = new ByteArrayInputStream(decryptedBuffer);


        return BitmapFactory.decodeByteArray(decryptedBuffer, 0, decryptedBuffer.length);
    }

}
